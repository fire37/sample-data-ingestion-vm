# script to separate the data and organize it by road, camera, and video
import os 
import csv 
import pandas as pd 

os.chdir("E:/Shared/Data/CVPR 2018 WAD Video Segmentation Challenge/train_video_list")

files = os.listdir()

for i in range(len(files)):
	f1 = files[i]
	f = pd.read_csv(f1, sep=" ", dtype='object', header=None)

	file_names = []
	file_dir = []
	road = f1[:6]
	cam = f1[7:12]
	video = f1[13:20]

	for i in range(len(f)):
		s = f.iloc[i][1]
		file_names.append(s[2:-34])
		file_dir.append('E:/Shared/Data/CVPR 2018 WAD Video Segmentation Challenge/train_color/train_color/' + s[2:-34])
